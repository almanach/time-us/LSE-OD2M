# -*- coding: utf-8 -*-
import argparse
import json
import os
from bs4 import BeautifulSoup
import kraken.rpred
import kraken.lib.models
from PIL import Image


MODEL = kraken.lib.models.load_any("./model_best.mlmodel") # cf. http://kraken.re/api.html#kraken.lib.models.load_any

# For test purpose:
TESTFILE = "./test_files/lesouvriersdesde01sociuoft_0051.xml"
TESTIMG = "./test_files/lesouvriersdesde01sociuoft_0051.tif"
TEST_blank = "./test_files/s2lesouvriersdes01sociuoft_0004.xml"
IMG_blank = "./test_files/s2lesouvriersdes01sociuoft_0004.tif"
TEST_illustration = "./test_files/s2lesouvriersdes01sociuoft_0027.xml"
IMG_illustration = "./test_files/s2lesouvriersdes01sociuoft_0027.tif"

#test = "./test_files/lesouvriersdesde01sociuoft_0236.xml"
#test = "./test_files/lesouvriersdesde01sociuoft_0116.xml"
#test = "./test_files/lesouvriersdesde01sociuoft_0026.xml"
#test = "./test_files/lesouvriersdesde01sociuoft_0012.xml"
#test = "./test_files/lesouvriersdesde01sociuoft_0011.xml"
test = "./test_files/lesouvriersdesde01sociuoft_0009.xml"

#image_test = "./test_files/lesouvriersdesde01sociuoft_0236.tif"
#image_test = "./test_files/lesouvriersdesde01sociuoft_0116.tif"
#image_test = "./test_files/lesouvriersdesde01sociuoft_0026.tif"
#image_test = "./test_files/lesouvriersdesde01sociuoft_0012.tif"
#image_test = "./test_files/lesouvriersdesde01sociuoft_0011.tif"
image_test = "./test_files/lesouvriersdesde01sociuoft_0009.tif"

# Shall we begin?
def sort_blocks(regions):
    regions.sort(key=lambda x:[int(item) for item in x.attrs['ID'].split('_')[1:]])
    return regions

class ParseError(Exception):
    pass


class SourceParser():
    def __init__(self, filename, image_loaded, image_filename, file_handler, page_number=0):
        self.filename = filename
        self.image = image_loaded
        self.img_file = image_filename
        self.soup = BeautifulSoup(file_handler, 'xml')
        self.page_number = page_number
        try:
            self.pages = self.soup.alto.Layout.find_all('Page')
            if len(self.pages) > 1:
                print("[WARNING]: this file contains more than 1 Page element - it will cause issues")
        except AttributeError:
            raise ParseError

    def lite_blocks(self):
        def table(tag):
            tag.name = 'graphic'
            #tag.attrs['TYPE'] = 'table'
            tag.clear() # remove children
            return tag

        def illustration(tag):
            tag.name = 'graphic'
            tag.attrs['TYPE'] = 'illustration'
            #tag.clear() # remove children
            return tag

        # ALTO exported from Transkribus normally only has one Page element!
        # for index, page in enumerate(self.pages):
        try:
            printspaces = self.pages[0].find_all('PrintSpace') # here we only consider the 1st item in "pages" because we don't loop
            # Because there shouldn't be more than one PrintSpace:
            if len(printspaces) > 1:
                print("[INFO]: Page {} contains more than 1 PrintSpace.".format("<place_holder>")) # get page number

            # Building a list of tags sorting text blocks from figures like illustrations and tables
            regions = []
            for printspace in printspaces:
                if len(printspace.contents) == 0: # then it's a blank page
                    print("[INFO]: Page {} is empty.".format("<place_holder>")) # get page number
                else:
                    for child in printspace.contents:
                        if child.name == 'Illustration':
                            regions.append(illustration(child))
                        elif child.name == 'ComposedBlock':
                            regions.append(table(child))
                        elif child.name == 'TextBlock':
                            regions.append(child)
                        elif not(child.name == None) and not(child.name == 'GraphicalElement'):
                            # add page name for easier localisation
                            try: # here we try because that unexpected element might not have an ID attribute nor any attr at all
                                id = child.attrs["ID"]
                            except:
                                id = "no available ID"
                            print("[INFO]: Unexpected tag named '{}' was ignored. ID = {}".format(child.name, id))
            not_text = [block for block in regions if block.name != "TextBlock"]
            if len(not_text) == 1:
                print("[INFO]: there is {} non-text block (graphic) in this page".format(len(not_text)))
            elif len(not_text) > 1:
                print("[INFO]: there are {} non-text blocks (graphic) in this page".format(len(not_text)))
            return regions  # regions can be an empty list!
        except AttributeError as e:
           raise ParseError(e)

    def remove_text_from_tables(self, blocks):
        # table zone is forbidden zone
        graphics = [block for block in blocks if block.name == 'graphic'] # defining forbidden zone
        forbidden_zones = []
        for g in graphics:
            box = {'top': int(g.attrs['VPOS']), 'bottom': int(g.attrs['VPOS']) + int(g.attrs['HEIGHT'])}
            forbidden_zones.append(box)
        allowed_blocks = []
        textblocks = [block for block in blocks if block.name != 'graphic']
        for block in textblocks: # should we apply this to textline level or textblock level?
            box = {'top': int(block.attrs['VPOS']),'bottom': int(block.attrs['VPOS']) + int(block.attrs['HEIGHT'])}
            checks = []
            for fz in forbidden_zones:
                ftop = int(fz['top'])
                fbottom = int(fz['bottom'])
                btop = int(box['top'])
                bbottom = int(box['bottom'])
                if ((btop < ftop) or (btop >= fbottom)) and ((bbottom <= ftop) or (bbottom > fbottom)):
                    checks.append(True)
                else:
                    checks.append(False)
            if not(checks.count(True) == len(checks)):
                print("[INFO]: Block {} in page {} is in a table zone".format(block.attrs['ID'], "<place_holder>")) # get page number
            else:
                allowed_blocks.append(block)
        print("[INFO]: out of {} text blocks, {} remain.".format(len(textblocks),len(allowed_blocks)))
        return allowed_blocks + graphics, len(allowed_blocks)

    def transcribe(self, block):
        def create_line_tag():
            line_tag = BeautifulSoup('<l></l>', 'xml')
            return line_tag.l

        def create_par_tag():
            par_tag = BeautifulSoup('<p></p>', "xml")
            return par_tag.p

        def bbox(tag):
            return [
                int(tag.attrs['HPOS']),
                int(tag.attrs['VPOS']),
                int(tag.attrs['HPOS']) + int(tag.attrs['WIDTH']),
                int(tag.attrs['VPOS']) + int(tag.attrs['HEIGHT'])
            ]

        def release_the_kraken(image, segments):
            # cf. http://kraken.re/api.html#kraken.rpred.rpred
            transcription = "".join([t.prediction for t in list(kraken.rpred.rpred(network=MODEL, im=image, bounds=segments))])
            return transcription

        par = create_par_tag()
        for textline in block.find_all('TextLine'):
            del textline['ID']  # useless attribute
            line_tag = create_line_tag()
            line_tag.attrs = textline.attrs
            segment = {'text_direction': 'horizontal-tb', 'boxes': [bbox(textline)], 'script_detection': False}
            line_tag.append(release_the_kraken(self.image, segment))
            par.append(line_tag)
        return par

    def initialize_page_tree(self):
        def create_page_tree():
            return BeautifulSoup('<div type="page"></div>', "xml")

        page_tree = create_page_tree().div
        # ALTO exported from Transkribus normally only has one Page element!
        for attr in self.pages[0].attrs:
            if attr == "HEIGHT" or attr == "WIDTH":
                page_tree[attr] = self.pages[0][attr]
        page_tree['img'] = self.img_file
        page_tree['topMargin'] = self.pages[0].TopMargin['HEIGHT']
        head, page_tree['ID'] = os.path.split(self.filename)
        return page_tree

# ------------

# get XML source file, get Image source file (XML name -.xml +.tif)

# Load source image
image_file = image_test
try:
    #image_source = Image.open('./s2lesouvriersdes01sociuoft_0027.tif')
    loaded_img = Image.open(image_file)
except IOError as e:
    loaded_img = False
    print('[ERROR]: {}'.format(str(e)))

if loaded_img:
    with open(test, "r") as fh:
        parser = SourceParser("this/is/my/filenamefortesting", loaded_img, image_file, fh)
    blocks = parser.lite_blocks()                       # simplifying page content
    if len(blocks) > 0:                                 # if it's not a blank page
        sorted_blocks, number_of_textblocks = parser.remove_text_from_tables(blocks) # sorting out textblocks located in table zones
        ordered_blocks = sort_blocks(sorted_blocks)
    else:
        ordered_blocks = []

    revised_page = parser.initialize_page_tree() # creating new tree
    for block in ordered_blocks:
        if block.name == 'graphic':
            revised_page.append(block)
        elif block.name == 'TextBlock':
            transcribed_block = parser.transcribe(block)
            transcribed_block.attrs = block.attrs
            revised_page.append(transcribed_block)
    for elem in revised_page.contents:
        print(elem.name)
        print(elem.attrs)

# to keep track of physical pages : do not keep "div type='page'" but add "pageX_" to blocks' ID !
# this "pageX_" will correspond to the numer of the page on which the logical block started !