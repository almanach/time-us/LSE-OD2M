# -*- coding: utf-8 -*-
import os
import unicodedata
from bs4 import BeautifulSoup
import kraken.rpred
import kraken.lib.models

VERBOSE = False  # if True, triggers a series of execution messages
MODEL = kraken.lib.models.load_any("../models/default_od2m.mlmodel")
# cf. http://kraken.re/api.html#kraken.lib.models.load_any


class ParseError(Exception):
    pass


class SourceParser:
    def __init__(self, filename: str, image: str, file_handler: str, image_file):
        self.filename = filename
        self.image = image
        self.soup = BeautifulSoup(file_handler, 'xml')
        self.image_file = image_file

        try:
            self.pages = self.soup.alto.Layout.find_all('Page')
            if len(self.pages) > 1:
                print("\n[WARNING]: this file contains more than 1 Page element - it will cause issues")
        except AttributeError:
            raise ParseError

    def lite_blocks(self) -> list:
        """Transform a parsed XML tree into a list of selected elements and return the list.

        :return: list of selected elements
        """
        def table(tag: BeautifulSoup) -> BeautifulSoup:
            """Transform a tag into <graphic type="table"> and return it"""
            tag.name = 'graphic'
            # tag.attrs['TYPE'] = 'table'
            tag.clear()  # remove children
            return tag

        def illustration(tag: BeautifulSoup) -> BeautifulSoup:
            """Transform a tag into <graphic type="illustration"> and return it"""
            tag.name = 'graphic'
            tag.attrs['TYPE'] = 'illustration'
            # tag.clear() # remove children
            return tag

        # ALTO exported from Transkribus normally only has one Page element!
        # for index, page in enumerate(self.pages):
        try:
            # here we only consider the 1st item in "pages" because we don't loop
            printspaces = self.pages[0].find_all('PrintSpace')
            # Because there shouldn't be more than one PrintSpace:
            if len(printspaces) > 1:
                print("[WARNING]: Page {} contains more than 1 PrintSpace.".format("<place_holder>"))  # get page number

            # Building a list of tags sorting text blocks from figures like illustrations and tables
            regions = []
            for printspace in printspaces:
                if len(printspace.contents) == 0:  # then it's a blank page
                    if VERBOSE is True:
                        print("[INFO]: Page {} is empty.".format("<place_holder>"))  # get page number
                else:
                    for child in printspace.contents:
                        if child.name == 'Illustration':
                            regions.append(illustration(child))
                        elif child.name == 'ComposedBlock':
                            regions.append(table(child))
                        elif child.name == 'TextBlock':
                            regions.append(child)
                        elif not(child.name is None) and not(child.name == 'GraphicalElement'):
                            # add page name for easier localisation
                            # here we try because that unexpected element might not have an ID attribute
                            # nor any attr at all:
                            try:
                                id = child.attrs["ID"]
                            except KeyError:
                                id = "no available ID"
                            print("[WARNING]: Unexpected tag named '{}' was ignored. ID = {}".format(child.name, id))
            not_text = [block for block in regions if block.name != "TextBlock"]
            if VERBOSE is True:
                if len(not_text) == 1:
                        print("[INFO]: there is {} non-text block (graphic) in this page".format(len(not_text)))
                elif len(not_text) > 1:
                    print("[INFO]: there are {} non-text blocks (graphic) in this page".format(len(not_text)))
            return regions  # regions can be an empty list!
        except AttributeError as e:
            raise ParseError(e)

    def remove_text_from_tables(self, blocks: list) -> list:
        """For a given list of elements, get rid of text block located in the same zone(s) as identified tables.

        :param blocks: list of XML elements
        :return: list of sorted XML elements
        """
        # not sure it is relevant to make it a method and not a function...
        # table zone is forbidden zone
        graphics = [block for block in blocks if block.name == 'graphic']  # defining forbidden zone
        forbidden_zones = []
        for g in graphics:
            box = {'top': int(float(g.attrs['VPOS'])), 'bottom': int(float(g.attrs['VPOS'])) + \
                                                                 int(float(g.attrs['HEIGHT']))}
            forbidden_zones.append(box)
        allowed_blocks = []
        textblocks = [block for block in blocks if block.name != 'graphic']
        for block in textblocks:  # should we apply this to textline level or textblock level?
            box = {'top': int(float(block.attrs['VPOS'])), 'bottom': int(float(block.attrs['VPOS'])) + \
                                                                    int(float(block.attrs['HEIGHT']))}
            checks = []
            for fz in forbidden_zones:
                ftop = int(float(fz['top']))
                fbottom = int(float(fz['bottom']))
                btop = int(float(box['top']))
                bbottom = int(float(box['bottom']))
                if ((btop < ftop) or (btop >= fbottom)) and ((bbottom <= ftop) or (bbottom > fbottom)):
                    checks.append(True)
                else:
                    checks.append(False)
            if not(checks.count(True) == len(checks)):
                if VERBOSE is True:
                    # get page number
                    print("[INFO]: Block {} in page {} is in a table zone".format(block.attrs['ID'], "<place_holder>"))
            else:
                allowed_blocks.append(block)
        if not (len(textblocks) == len(allowed_blocks)):
            if VERBOSE is True:
                print("[INFO]: out of {} text blocks, {} remain.".format(len(textblocks), len(allowed_blocks)))
        return allowed_blocks + graphics

    def transcribe(self, block: BeautifulSoup) -> BeautifulSoup:
        """Proceed to transcription of the content of a given block of text.

        :param block: parsed XML tag
        :return: <p> XML element containing transcribed lines in <l> XML elements
        """
        def create_line_tag() -> BeautifulSoup:
            """Return a <l> tag."""
            line_tag = BeautifulSoup('<l></l>', 'xml')
            return line_tag.l

        def create_par_tag() -> BeautifulSoup:
            """Return a <p> tag."""
            par_tag = BeautifulSoup('<p></p>', "xml")
            return par_tag.p

        def bbox(tag: BeautifulSoup) -> list:
            """Define bounding box of a line of text from a given tag's coordinates."""
            return [
                int(tag.attrs['HPOS']),
                int(tag.attrs['VPOS']),
                int(tag.attrs['HPOS']) + int(tag.attrs['WIDTH']),
                int(tag.attrs['VPOS']) + int(tag.attrs['HEIGHT'])
            ]

        def release_the_kraken(im:str, segments: dict, image_name: str, test=False, normalize_nfc=True) -> str:
            """Call Kraken's transcription function and return transcription."""
            # cf. http://kraken.re/api.html#kraken.rpred.rpred
            transcription = "".join([t.prediction for t in list(kraken.rpred.rpred(network=MODEL,
                                                                                   im=im, bounds=segments))])  # - FLAG!

            if test is True:
                print("[TEST]: segment = ".format(segments))
                print("[TEST]: read '{}'".format(transcription))
            if len(transcription.replace(" ", "")) == 0:
                print("\n[WARNING]: on image {}: could not read content of box {}".format(
                    image_name.split(os.sep)[-1].replace(".tif",""), segment['boxes']))
            if test is True:
                next(kraken.rpred.extract_boxes(im, bounds=segment))[0].show()  # - - - - - - - - - - - - - - - - FLAG!
            if normalize_nfc:
                transcription = unicodedata.normalize('NFC', transcription)
                transcription = unicodedata.normalize('NFC', transcription)
            return transcription

        par = create_par_tag()
        for textline in block.find_all('TextLine'):
            del textline['ID']  # useless attribute
            line_tag = create_line_tag()
            line_tag.attrs = textline.attrs
            segment = {'text_direction': 'horizontal-tb', 'boxes': [bbox(textline)], 'script_detection': False}
            line_tag.append(release_the_kraken(self.image, segment, self.filename.replace(".xml", "")))
            par.append(line_tag)
        return par

    def initialize_page_tree(self) -> BeautifulSoup:
        """Start an XML tree for output."""
        def create_page_tree() -> BeautifulSoup:
            """Return <div> tag."""
            return BeautifulSoup('<div type="page"></div>', "xml")

        page_tree = create_page_tree().div
        # ALTO exported from Transkribus normally only has one Page element!
        for attr in self.pages[0].attrs:
            if attr == "HEIGHT" or attr == "WIDTH":
                page_tree[attr] = self.pages[0][attr]
        page_tree['img'] = self.image_file
        page_tree['topMargin'] = self.pages[0].TopMargin['HEIGHT']
        head, page_tree['ID'] = os.path.split(self.filename)
        return page_tree

