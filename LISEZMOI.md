# Extraction de la Structure Logique pour *Les Ouvriers des Deux Mondes* (LSE-0D2M)

## Description

### Structure du dossier
.  
├── archives/   
│   ├── processing_alto_individually  
│   │   └── {scripts obsolètes}  
│   ├── processing_xml_abby_from_sharedocs/     
│   │   └── {scripts obsolètes}  
├── docs/  
│   ├── exploratory_jupyter/  
│   │   └── {jupyter notebooks pour tests préalables}  
│   ├── pipeline/  
│   │   └── {schémas pour documenter la pipeline}  
│   ├── pre_processing_images/  
│   │   └── {schémas et captures d'écran sur les étapes de pré-traitement d'images}  
│   └── processus.odt # notes (brouillon) sur le projet pour garder la trace de certaines décisions  
├── data_set/  
├── models/ 
│   └── default_od2m.mlmodel   
├── build_structured_transcription/ # code principale  
│   ├── lse_classes/  
│   │   ├── \_\_init\_\_.py  
│   │   ├── altoparse_class.py  
│   │   └── teiparse_class.py  
│   ├── lse_structure/  
│   │   ├── \_\_init\_\_.py  
│   │   └── structure.py  
│   ├── lse_utils/  
│   │   ├── \_\_init\_\_.py  
│   │   ├── lse_debug.py  
│   │   └──  lse_utils.py  
│   ├── create_html_output.py  
│   ├── main.py  
│   ├── readme.md  
│   ├── reference_headers.json  
│   └── reference_sections.json  
├── .gitignore  
├── LISEZMOI.md  
├── README.md  
└── requirements.txt

## Pré-requis


## Installation

## Exécution

### Options
