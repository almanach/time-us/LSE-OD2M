# -*- coding: utf-8 -*-
import time

from bs4 import BeautifulSoup
import os
from tqdm import tqdm

from lse_utils import lse_utils as lseUtils
from lse_utils import lse_debug as lseDebug
from lse_classes import altoparse_class as altoParse
from lse_classes import teiparse_class as teiParse
from lse_structure import structure


def get_transcription(pairs: list) -> list:
    """Parse XML files, sort blocks transcribe text blocks and return a list of PageParser objects.

    :param pairs: list of tuples where tuple is (xml file, image file)
    :return: list of PageParser objects
    """
    # N.B.: This step is very dependant on the format of the provided XML ALTO file(s).
    # It has been designed for XML ALTO exported from Transkribus after running the ABBYY OCR tool
    # ABBY OCR tool produces segmentation, classification of segments and transcription
    # we do care about the segmentation and the classification
    # we don't care about the transcription

    def create_content_of_xml_tree(page, blocks):
        """Process a list of XML elements (blocks) and add them to a container (page) after transcribing
        'TextBlock' type tags."""
        for block in ordered_blocks:
            if block.name == "graphic":
                page.append(block)
            elif block.name == "TextBlock":
                # TextBlocks are to be transcribed with kraken
                transcribed_block = original_alto.transcribe(block)
                # and then we keep their attributes in the new XML element
                transcribed_block.attrs = block.attrs
                page.append(transcribed_block)
        return page

    volume = []
    # strong verbosity defeats the purpose of a progressbar
    if opt_verbose is True:
        list_of_pairs = pairs
    else:
        list_of_pairs = tqdm(pairs, desc="Transcription", unit="image", ncols=100, leave=False)
    for alto_file, image_file in list_of_pairs:
        # Loading source image
        image = lseUtils.load_image(image_file)
        if image:
            if opt_verbose is True:
                print("[TEST]: FILE: {}".format(image_file.replace(".tif", "")))
            # Parsing XML file
            with open(alto_file, "r") as fh:
                original_alto = altoParse.SourceParser(filename=alto_file, image=image, file_handler=fh, image_file=image_file)

            # sorting blocks to only keep text blocks and relevant graphic elements (ex: table, illustration)
            blocks = original_alto.lite_blocks()
            # if it's not a blank page, we go further
            if len(blocks) > 0:
                # steps consist of ignoring text blocks located in table zones
                # and then redefining blocks order depenfing on their ID attribute
                sorted_blocks = original_alto.remove_text_from_tables(blocks)
                ordered_blocks = lseUtils.sort_blocks(sorted_blocks)
            else:
                ordered_blocks = []

            # creating new XML tree - there will be one XML tree per page
            # stored in a PageParser object
            revised_page = original_alto.initialize_page_tree()
            create_content_of_xml_tree(revised_page, ordered_blocks)
            # 'volume' will hold all pages in the documents, as PageParser objects
            page = teiParse.PageParser(image=revised_page["img"], alto=alto_file,
                                       handler=revised_page)  # is this necessary ?
            volume.append(page)
    return volume


def get_chapters(volume: list) -> list:
    """Detect chapters in a volume based on different sets of rules.

    :param volume: list of PageParser objects
    :return: list of lists of PageParser objects
    """
    # We first detect investigation title pages because they have a stable textual pattern
    # N.B.: this step can be improved to rely on layout analysis rather than textual analysis. - - - - - - - - - - FLAG!
    for page in tqdm(volume, desc="Retrieving title pages", unit="page", ncols=100, leave=False):
        if len(page.handler.contents) == 0:
            page.is_empty = True
        else:
            structure.classify_first_page(page)
    if opt_debug is True:
        lseDebug.debug_spotting_titles(volume)

    # Once we spotted investigation's title page, we get a first state of chapters detection but we are missing
    # the detection of chapters in the front and the back matter
    sliced_volume = structure.slice_volume_in_chapters(volume)
    # first item is "front matter", last item is last investigation + "back matter"
    if opt_debug is True:
        lseDebug.debug_building_chapter(sliced_volume)

    # if there is more than 1 slice (chapter) detected in the volume, we distinguish front from back matter
    # in order to avoid duplicating chapters
    if len(sliced_volume) > 1:
        print("[INFO]: now slicing front and back matter")
        # slicing front matter (sliced_volume[0]) and then back matter (sliced_volume[-1])
        front = structure.titles_in_front_and_back_matter(sliced_volume[0].content)
        sliced_front = structure.slice_volume_in_chapters(front)  # a list of ChapterHandler objects
        back = structure.titles_in_front_and_back_matter(sliced_volume[-1].content)
        sliced_back = structure.slice_volume_in_chapters(back)  # a list of ChapterHandler objects
        # getting everything back together!
        chapters = sliced_front + sliced_volume[1:-1] + sliced_back
    else:
        chapters = structure.slice_volume_in_chapters(structure.titles_in_front_and_back_matter(volume))
    if opt_debug is True:
        lseDebug.debug_how_many_chapters(chapters)
    return chapters


# = = = = = = = = = = = = = =
# = = = = = SCRIPT  = = = = =
# = = = = = = = = = = = = = =

# - - - - - PARSING CLI OPTIONS - - - - -
import argparse
parser = argparse.ArgumentParser(description='Takes images and segmentation data to produce structured TEI-XML transcriptions.')
parser.add_argument('-i', '--input', action='store', nargs=1, required=True, help='absolute path to load input.')
parser.add_argument('-o', '--output', action='store', nargs=1, help='absolute path to save output.')
parser.add_argument('--scratch', action='store_true', help='start from images and performs transcription before structuration. Expect directory as input.')
parser.add_argument('--transcr', action='store_true', help='start from transcriptions and perform structuration. Expect json file as input.')
parser.add_argument('--debug', action='store_true', help='activate debug mode.')
parser.add_argument('--verbosity', action='store_true', help='activate verbosity mode.')
parser.add_argument('--notiming', action='store_true', help='deactivate execution timing.')
args = parser.parse_args()

# Do we start from scratch or from transcriptions ?
# if opt_from_scratch, will run script from the beginning (will skip step. 4)
# if opt_from_transcription, will run script from loading transcriptions (step.4)
opt_from_scratch, opt_from_transcription = lseUtils.interpret_starting_point(vars(args))
if lseUtils.verify_input_info(vars(args), opt_from_scratch):
    input_dir_to_load, input_json_to_load = lseUtils.interpret_input_info(vars(args), opt_from_scratch)
    output_location = lseUtils.verify_output_info(vars(args))

    # Reporting options:
    opt_debug = vars(args)['debug']  # if True: trigger a series of execution messages useful for debugging
    opt_notiming = vars(args)['notiming']  # if False: triggers the display of timing informations
    opt_verbose = vars(args)['verbosity']  # if True: triggers a series of execution messages

    # - - - - - STARTING TO WORK - - - - -
    start_script_execution = time.time()  # for timing
    if opt_from_scratch is True:
        # 1. Building pairs
        pairs = lseUtils.load_input(input_dir_to_load)  # pairs of pages
        if opt_verbose is True:
            print("[INFO]: loaded files from {}".format(input_dir_to_load))
            print("[INFO]: there are {} pairs.".format(len(pairs)))

        #pairs = pairs[75:100]  # for test purpose - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - FLAG!

        # 2. Transcribing pages
        print("\n[INFO]: starting transcription of the volume!")
        start_build_volume = time.time()
        volume = get_transcription(pairs)
        end_build_volume = time.time()

        # 3. building intermediary transcription:
        lseUtils.save_pages_as_json(output_location, volume, source_name=vars(args)['input'][0])

    if opt_from_transcription is True:
        # 4. load transcriptions as list of PageParser objects
        start_build_volume = time.time()
        volume = lseUtils.load_pages_as_json(input_json_to_load)
        volume = lseUtils.normalize_strings_nfc(volume)
        end_build_volume = time.time()

        #volume = volume[50:55]  # for test purpose - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - FLAG!

    if len(volume) > 0:
        # 4.1 Remove empty "p" from pages (aka cleaning)
        volume = structure.remove_empty_p_in_page(volume)
        # 5. build chapters
        print("\n[INFO]: starting identifying chapters in the volume!")
        start_building_chapters = time.time()
        chapters = get_chapters(volume)

        if not chapters:
            end_building_chapters = time.time()

        else:
            # Step 6 is largely perfectible!
            # 6. Extract headers and pagination
            # the retrieval of headers could be thought differently and improved for more automation
            chapters = structure.get_page_and_headers(chapters)
            # imperfect recomposition of physical page numbers
            chapters = structure.build_pagination(chapters)

            # 7. get rid of tables in budget sections
            structure.sort_out_tables_from_budget_sections(chapters)
            end_building_chapters = time.time()

            # 8. Build TEI tree containing everything
            start_building_tei = time.time()
            pbar = tqdm(total=13, desc='TEI modeling', unit='step', ncols=100)
            # 8.1 build the TEI tree from combining all the pages' XML tree
            tei_tree = lseUtils.build_tei_export(chapters)
            pbar.update(1)  # pbar1
            # 8.2 build chapter's sections IDs
            tei_tree = structure.build_chapters_id(tei_tree)
            pbar.update(1)  # pbar2
            # 8.3 rebuild paragraphs spread across pages
            tei_tree = structure.handle_footnotes(tei_tree)  # for now, this will remove blocks identified as footnotes -- FLAG!
            pbar.update(1)  # pbar3
            # 8.4 remove tags marking lines
            tei_tree = structure.remove_lb_tags(tei_tree)
            pbar.update(1)  # pbar4
            # 8.5 resolve hyphenations, leaving a '¬' for landmark
            tei_tree = structure.resolve_hyphenations(tei_tree)
            pbar.update(1)  # pbar5
            # 8.6 build IDs for tables, paragraphs and other figures
            tei_tree = structure.build_pagecontent_ids(tei_tree)
            pbar.update(1)  # pbar6
            # 8.7 build <zone> in <facsimile>
            tei_tree = structure.create_zones_in_facsimile(tei_tree)
            pbar.update(1)  # pbar7
            # 8.8 unfold page content to lessen the mark of physical
            tei_tree = structure.transform_pagediv_into_pb(tei_tree)
            pbar.update(1)  # pbar8
            # 8.9 build embedded divs to render section/subsection/subsubsections structure within chapters
            tei_tree = structure.find_logical_structure_in_investigations(tei_tree)
            pbar.update(1)  # pbar9
            #tei_tree = structure.resolve_hyphenations_(tei_tree)

            # 9 final smallfix in TEI tree
            tei_tree = structure.delete_attributes_in_paragraphs(tei_tree)  # removes unnecessary attributes in <p> from body
            # 9.1 rename section titles elements as <head>
            tei_tree = structure.create_heads_in_sections(tei_tree)
            # 9.2 normalize @id attributes
            tei_tree = structure.normalize_id_attributes(tei_tree)  # this step might not remain
            pbar.update(1)  # pbar10
            # 9.3 removes unnecessary attributes in <graphic> from body
            tei_tree = structure.delete_attributes_in_graphics(tei_tree)
            # it might be more TEI-esque to call these elements 'figure"?
            pbar.update(1)  # pbar11
            # 9.4 removes unnecessary attributes in <pb> from body
            tei_tree = structure.delete_attributes_in_pagebeginings(tei_tree)
            pbar.update(1)  # pbar12
            # 9.5 this last bit is due to an annoying subtlety from bs4:
            # -> BS4 can"t handle tags called 'text" so we named it 'text_temp' as long as we parse the tree
            tei_tree.TEI.text_temp.name = "text"
            pbar.update(1)  # pbar13
            end_building_tei = time.time()

            # 10. Build a plain text export
            # This is just a small block for when we want to get only the text content of the document
            damn_text = lseUtils.just_get_me_the_damn_text(tei_tree)
    else:
        end_building_chapters = time.time()
        tei_tree = False


    if tei_tree:
        # 11. Create output!
        lseUtils.save_output(tei_tree, output_location)
        # lseUtils.save_output(damn_text, directory_of_input, mode="text")

    # FOR INFORMATION
    print("\n[INFO]: Finished!")
    end_script_execution = time.time()
    if opt_notiming is False:
        try:
            print("[TIME]: Transcription or loading pages took {:.2f} seconds".format(end_build_volume - start_build_volume))
            print("[TIME]: Getting chapters took {:.2f} seconds".format(end_building_chapters - start_building_chapters))
            if chapters:
                print("[TIME]: Building TEI took {:.2f} seconds".format(end_building_tei - start_building_tei))
            execution_time = end_script_execution - start_script_execution
            print("[TIME]: Execution time: {:.2f} seconds (which is roughly {:.1f} minutes)".format(
                execution_time,execution_time/60.0))
        except NameError as e:
            print("[OOPSY]: Something went wrong with displaying our program's execution time.")
else:
    print('[ERROR]: Exiting program. Input and behavior mode don\'t match.')
