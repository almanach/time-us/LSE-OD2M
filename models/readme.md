# Model description

## default_od2m.mlmodel
### training set
{describe the training set}

### transcription rules
{describe rules for the generation of ground truth}

### efficiency
{describe model efficiency}