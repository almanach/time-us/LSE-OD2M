# -*- coding: utf-8 -*-
import glob
import json
import numpy as np
import os
import unicodedata

from bs4 import BeautifulSoup
import stringdist
import kraken.binarization
from kraken.lib.util import pil2array

from PIL import Image
from tqdm import tqdm

from lse_classes import teiparse_class as teiParse


VERBOSE = False  # if True, triggers a series of execution messages
DEBUG = False


def get_iterator_length(iterator) -> int:
    """Return the length of an iterator"""
    return sum(1 for item in iterator)


def slice_a_list(list_to_slice: list, stopping_at: int, starting_at: int) -> list:
    """Split a list according to beginning and end indexes"""
    return list_to_slice[starting_at:stopping_at]


def save_pages_as_json(path: str, volume: list, source_name='default_transcription') -> None:
    """Serialize a list of PageParser objects and save in a file."""
    def build_json_filename(source_name, path):
        if source_name.endswith(os.sep):
            source_name = source_name[:-1]
        source_name = source_name.split(os.sep)[-1]
        return os.path.join(path, "{}.json".format(source_name))

    destination_json = build_json_filename(source_name, path)
    pages_contents = []
    for page in volume:
        page.handler = str(page.handler)
        pages_contents.append(page.__dict__)
    try:
        with open(destination_json, "w", encoding="utf8") as fp:
            json.dump(pages_contents, fp, ensure_ascii=True)
    except Exception as e:
        print("[WARNING]: something went wrong with dumping the transcriptions in the JSON files!")
    # this step is necessary to make handler parse XML again
    for page in volume:
        page.handler = BeautifulSoup(page.handler, "xml")


def load_pages_as_json(path: str) -> list:
    """Load a file containing PageParser objects and return a list."""
    try:
        with open(path, 'r') as fh:
            pages = json.load(fh)
    except Exception as e:
        print(e)
        return []
    try:
        volume = []
        for page in tqdm(pages, desc="Loading pages", unit="page", ncols=100):
            handler = BeautifulSoup(page['handler'], 'xml')
            volume.append(teiParse.PageParser(page['image'], page['alto'], handler, page['has_title'], page['type_of_title'], page['is_empty'], page['number'], page['header']))
        return volume
    except Exception as e:
        print(e)
        return []


def load_input(path: str) -> list:
    """Get every XML files in directory and build matching XML-IMG pairs.

    :param path: path to directory containing xml and image files
    :return: list of tuples where a tuple is (xml file, image file)
    """
    # get every XML files and then every TIF files
    xml_path = os.path.join(path, os.path.join("*", "*.xml"))
    xml_files = glob.glob(xml_path, recursive=True)
    tif_path = os.path.join(path, os.path.join("*", "*.tif"))
    tif_files = glob.glob(tif_path, recursive=True)
    # sort found XML files assuming rank is included in last part of file name after "_"
    xml_files.sort(key=lambda x: [int(item) for item in x.split("_")[-1].replace(".xml", "")])
    # building pairs
    pairs = []
    for xml_file in xml_files:
        xml_file_name, ext = xml_file.split(os.sep)[-1].split(".")
        found = False
        for tif_file in tif_files:
            if xml_file_name in tif_file:
                found = True
                break
        if found:
            pairs.append((xml_file, tif_file))
        else:
            print("[WARN]: Did not find image for {}".format(xml_file))
    return pairs


def load_image(filename: str) -> Image:
    """Parse an image file and return the result."""

    def test_if_empty_page(im):
        raw = pil2array(im)
        raw = raw / np.float(np.iinfo(raw.dtype).max)
        if np.amax(raw) == np.amin(raw):
            return True
        else:
            return False

    try:
        loaded_image = Image.open(filename)
    except Exception as e:
        print("[ERROR]: {}".format(e))
    # avoiding 'empty page' error from kraken binarization
    if not test_if_empty_page(loaded_image):
        try:
            loaded_image = kraken.binarization.nlbin(im=loaded_image)
        except IOError as e:
            print("[ERROR]: {}".format(str(e)))
    return loaded_image


def sort_blocks(regions: list) -> list:
    """Arrange elements in a list in ascending order depending on their ID attribute and return the list."""
    regions.sort(key=lambda x: [int(item) for item in x.attrs['ID'].split('_')[1:]])
    return regions


def they_are_different(truth: str, submission: str, threshold=10) -> list:
    """Compare two 2 strings' levenshtein distance and return True if different and distance.

    :param truth: expected string to compare to
    :param submission: string we want to compare to model
    :param threshold: maximum difference between the two string
    :return: True if string are different, False if not, and found distance -> [bool, int]
    """
    # normalizing the strings before comparing
    comparison_distance = stringdist.levenshtein(submission.replace(" ", "").lower(), truth.replace(" ", "").lower())
    if comparison_distance < threshold:
        different = False
    else:
        different = True
    if VERBOSE is True:
        if different is False:
            print("[TEST]: submission = {}\ntruth = {}\ndistance = {}".format(submission, truth, comparison_distance))
    return [different, comparison_distance]


def build_tei_export(chapters: list) -> BeautifulSoup:
    """Build a TEI XML tree with texts, facsimiles and pointers from texts to facsimiles."""
    def create_facsimile(tei_tree: BeautifulSoup) -> list:
        """Build a list of facsimile/surface/graphic/zone ensembles with necessary attributes for each page."""
        all_facsimile = []
        for page in tei_tree.find_all('div'):
            if 'PAGENB' in page.attrs:
                new_facsimile = BeautifulSoup('<facsimile><surface><graphic/><zone rendition="printspace"/></surface></facsimile>', 'xml')
                new_facsimile.surface.attrs = {'lry': page.attrs['HEIGHT'], 'ury': 0, 'urx': 0, 'lrx': page.attrs['WIDTH']}
                new_facsimile.surface.graphic.attrs = {'url': page.attrs['img']}
                all_facsimile.append(new_facsimile.facsimile)
        return all_facsimile

    def link_pages_to_facs(tei_tree: BeautifulSoup) -> BeautifulSoup:
        """Build @facs into <div> pointing to corresponding //facsimile/surface/graphic element."""
        for page in tei_tree.find_all("div"):
            if 'PAGENB' in page.attrs:
                matching_facs_result = [match for match in tei_tree.find_all("facsimile") if page.attrs["ID"].replace(".xml","") in match.surface.graphic.attrs["url"]]
                matching_facs = matching_facs_result[0]  # normally there is only one occurrence in the tree
                page.attrs["facs"] = "#{}".format(matching_facs.attrs["xml:id"])
        return tei_tree

    def give_xmlid_to_facs(all_facsimile: list) -> list:
        """Build @xml:id into <facs> elements based on a counter."""
        facs_counter = 1
        for facsimile in all_facsimile:
            facsimile["xml:id"] = "facs_{}".format(facs_counter)
            facs_counter += 1
        return all_facsimile

    def resolve_hyphenations(paragraph: BeautifulSoup) -> None:
        """Locate traces of hyphenations and resolve them by joining both parts with '¬' character."""
        paragraph_content = ""
        for line in paragraph.contents:
            paragraph_content = paragraph_content + line.strip()
            if paragraph_content[-1] == "-":
                paragraph_content = paragraph_content[:-1] + "¬"
            else:
                paragraph_content = paragraph_content + " "
        paragraph.clear()
        paragraph.append(paragraph_content)

    def change_line_into_lb_in_paragraph(paragraph: BeautifulSoup) -> None:
        """Change <line> into succesion of <lb> and text in a given paragraph."""
        for line in paragraph.find_all("l"):
            if line.string:
                line.insert_after(line.string)
            line.clear()
            line.name = "lb"

    tei_header_basis = """<TEI><teiHeader><fileDesc><titleStmt><title><!-- title of the resource --></title></titleStmt>
        <publicationStmt><p><!-- Information about distribution of the resource --></p></publicationStmt>
        <sourceDesc><p><!-- Information about source from which the resource derives --></p></sourceDesc></fileDesc></teiHeader>
        <text_temp><body></body></text_temp></TEI>"""
    volume_as_tei = BeautifulSoup(tei_header_basis, "xml")
    for chapter in chapters:
        new_chapter = volume_as_tei.new_tag('div', type='chapter')
        for page in chapter.content:
            # transfering pagination to page div!
            page.handler.div.attrs["PAGENB"] = str(page.number)
            if int(page.handler.div.attrs["PAGENB"]) < 1:  # if it's -1 or 0
                page.handler.div.attrs["PAGENB"] = ""
            new_chapter.append(page.handler)
        volume_as_tei.TEI.text_temp.body.append(new_chapter)
    for p in volume_as_tei.text_temp.body.find_all("p"):
        if len(p.contents) == 0:
            trash = p.extract()
        else:
            change_line_into_lb_in_paragraph(p)
    all_facsimile = create_facsimile(volume_as_tei)
    give_xmlid_to_facs(all_facsimile)
    for facsimile in all_facsimile:
        volume_as_tei.text_temp.insert_before(facsimile)
    volume_as_tei = link_pages_to_facs(volume_as_tei)
    return volume_as_tei


def normalize_strings_nfc(volume: list) -> list:
    """Process a list of pageParser objects and normalize the text in <l>s to NFC."""
    for page in volume:
        all_lines = page.handler.find_all("l")
        for line in all_lines:
            if line.string:
                line.string = unicodedata.normalize('NFC', line.string)
    return volume


def just_get_me_the_damn_text(tei_tree: BeautifulSoup) -> str:
    """Extract all the text in TEI tree respecting paragraphs and return it as a string."""
    brutal_text_retrieval = ""
    for para in tei_tree.body.find_all('p'):
        brutal_text_retrieval = brutal_text_retrieval + para.get_text() + "\n"
    return brutal_text_retrieval


def save_output(content, output_location: str, mode="xml", filename="default") -> None:
    """Create a file and write output in it.

    :param content: parsed XML or plain string
    :param output_location: path given to access input data
    :param mode: string, either 'xml' for XML either anything else for text
    :param filename: string, desired name for output file.
    :return: None
    """
    if mode.lower() == 'xml':
        path_to_output_file = os.path.join(output_location, "{}.xml".format(filename))
        content = content.prettify()
    else:
        path_to_output_file = os.path.join(output_location, "{}.txt".format(filename))
        if isinstance(content, str):
            print('[WARNING]: content to save in output is not `str` but {}'.format(type(content)))
    try:
        with open(path_to_output_file, 'w', encoding='utf8') as f:
            f.write(content)
    except Exception as e:
        print('[ERROR]: Failed to save the document.')
        print(e)


def create_directory(directory: str) -> None:
    """Create a new directory."""
    if not os.path.exists(directory):
        os.makedirs(directory)


def interpret_starting_point(args: dict) -> tuple:
    """Parse args from CLI and return tuple (opt_from_scratch, opt_from_transc)."""
    # if opt_starting_from_transcriptions is True :
    # if opt_starting_from_scratch is True :
    # it's useless for both to be True, so default behavior is both are given in CLI is
    # to only set opt_start_from_scratch to True
    if args['scratch'] is True and args['transcr'] is True:
        return True, False
    elif args['scratch'] is False and args['transcr'] is False:
        print('[WARNING]: no starting point specified (--transc, --scratch): activating --scratch behavior.')
        return True, False
    elif args['scratch'] is True:
        return True, False
    elif args['transcr'] is True:
        return False, True


def verify_input_info(args: dict, starting_from_scratch: bool) -> bool:
    """Test the validity of the path and format of input:
    - if we start from scratch, input must be a directory.
    - if we start from transcriptions, input must be a JSON file.

    :param args: parsed arguments from CLI
    :param starting_from_scratch: True if we start from scratch, False if we start from transcriptions
    :return: True if input is valid; display a message an return False if input is not valid.
    """
    arg_input_value = args['input'][0]
    if starting_from_scratch is True:
        if os.path.isdir(arg_input_value):
            return True
    else:
        if os.path.isfile(arg_input_value) and arg_input_value.endswith('.json'):
            return True
        else:
            print('[ERROR]: input must be an absolute path to an existing directory or an existing JSON file.')
            return False


def interpret_input_info(args: dict, starting_from_scratch: bool) -> tuple:
    """Distribute input path to either the directory option (1) or the json option (2)"""
    if starting_from_scratch is True:  # if we are supposed to load a directory...
        return args['input'][0], False  # directory option receives the path, JSON option receives nothing
    else:  # if on the contrary we are supposed to load a JSON file...
        return False, args['input'][0]  # directory option receives nothing, JSON option receives the path


def verify_output_info(args: dict) -> str:
    default_output_path = os.path.join(os.path.expanduser('~'), 'structured_transcription')
    if args['output']:
        arg_output_value = args['output'][0]
        if os.path.exists(arg_output_value):
            if os.path.isdir(arg_output_value):
                return os.path.abspath(arg_output_value)
            else:
                if arg_output_value[-1] == os.sep:
                    arg_output_value = arg_output_value[:-1]
                if '.' in arg_output_value.split(os.sep)[-1]:
                    print('\n[WARNING]: location of output must be a directory. Default saving to: {}'.format(default_output_path))
                    create_directory(default_output_path)
                    return default_output_path
        else:
            print('\n[INFO]: created new directory to save output: {}'.format(os.path.abspath(arg_output_value)))
            create_directory(arg_output_value)
            return os.path.abspath(arg_output_value)
    else:
        print('\n[INFO]: no specified location for output. Default saving to: {}'.format(default_output_path))
        create_directory(default_output_path)
        return default_output_path
