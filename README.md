# Logical Structure Extraction from *Les Ouvriers des Deux Mondes* (LSE-OD2M)

## Description


### Folder structure
.  
├── archives/   
│   ├── processing_alto_individually  
│   │   └── {obsolete scripts}  
│   ├── processing_xml_abby_from_sharedocs/     
│   │   └── {obsolete scripts}  
├── docs/  
│   ├── exploratory_jupyter/  
│   │   └── {jupyter notebooks for testing algos}  
│   ├── pipeline/  
│   │   └── {diagrams to document the pipeline}  
│   ├── pre_processing_images/  
│   │   └── {diagrams and screenshots on image preprocessing steps}    
│   └── processus.odt # drafty notes on the project to keep track of decisions  
├── data_set/  
├── models/ 
│   └── default_od2m.mlmodel  
├── build_structured_transcription/ # main code  
│   ├── lse_classes/  
│   │   ├── \_\_init\_\_.py  
│   │   ├── altoparse_class.py  
│   │   └── teiparse_class.py  
│   ├── lse_structure/  
│   │   ├── \_\_init\_\_.py  
│   │   └── structure.py  
│   ├── lse_utils/  
│   │   ├── \_\_init\_\_.py  
│   │   ├── lse_debug.py  
│   │   └──  lse_utils.py  
│   ├── create_html_output.py  
│   ├── main.py   
│   ├── readme.md  
│   ├── reference_headers.json  
│   └── reference_sections.json  
├── .gitignore  
├── LISEZMOI.md  
├── README.md  
└── requirements.txt

## Installing
1. create a **python 3** virtual environment
2. install [`kraken`](http://kraken.re/#installation) in the environment
3. with virtual environment activated, run:  
 ``` bash
 pip install -r requirements.txt
 ```

## Requirements
In order to perform the transcription step, **you need a kraken model [trained](http://kraken.re/ketos.html) on the set of images you wish to process** using LSE-OD2M.  

We provide a default model trained on binarized digitizations of pages from various volumes of Le Play's monographies \[[more information on default model](link_to_models)\].

To specify another transcription model, you can assigne the `MODEL` constant in [`altoparse_class.py`](link_to_altoparse) a different value.

``` python
# altoparse_class.py
...
MODEL = kraken.lib.models.load_any("../models/default_od2m.mlmodel")
# cf. http://kraken.re/api.html#kraken.lib.models.load_any
...

                       |
                       |
                       v
                       
# modified path to model
...
MODEL = kraken.lib.models.load_any("~/user/mymodel.mlmodel")
# cf. http://kraken.re/api.html#kraken.lib.models.load_any
...
```


## Running
Running LSE-0S2M is fairly simple :

``` bash
python3 main.py -i directory/containing/alto_and_images/ --scratch
```

Such a command as above will load images and ALTO XML files to perform transcription, and produce an intermediary JSON file containing the transcriptions, and run the structuration steps. Adding `--scratch` is not necessary, since this mode is the default mode.  

Transcribing a volume of about 500 pages can take up to 1 hour. For this reason, if you are satisfied with the transcription but need to perform again the structuration of the transcription, you may use the option `--transcr`:

``` bash
python3 main.py -i path/to/transcriptions/as/json_format/ --transcr
```

### Options
Several options are available at all time:
- `--output` / `-o` : use to specify a folder in which output (json and xml) will be saved. If `--output` option is not used, files will be saved to `~/structured_transcription/`.

\[will change soon :arrow_lower_right:\]  
- `--debug`: to display debug information.
- `--verbosity`: to display additional information.
- `--notiming`: to prevent script from displaying information about timing.
