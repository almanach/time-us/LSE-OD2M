# -*- coding: utf-8 -*-
import argparse
import json
from bs4 import BeautifulSoup


class ParseError(Exception):
    pass

class AltoParser():
    def __init__(self, file_handler):
        self.soup = BeautifulSoup(file_handler, 'xml')
        try:
            self.pages = self.soup.alto.Layout.find_all('Page')
        except AttributeError:
            raise ParseError

    def tojson(self):
        def bbox(tag):
            return (
                int(tag.attrs['HPOS']),
                int(tag.attrs['VPOS']),
                int(tag.attrs['HPOS']) + int(tag.attrs['WIDTH']),
                int(tag.attrs['VPOS']) + int(tag.attrs['HEIGHT'])
            )

        try:
            for index, page in enumerate(self.pages):
                regions = []
                for block in page.find_all('TextBlock'):
                    for line in block.find_all('TextLine'):
                        regions.append(bbox(line))
            segments = {'text_direction':'horizontal-tb','boxes':regions, 'script_detection': False}
            return json.dumps(segments)
        except AttributeError as e:
            raise ParseError(e)

# ----

parser = argparse.ArgumentParser(description='Transform XML ALTO files into JSON segments for Kraken')
parser.add_argument('-i', action='store', required=True, nargs=1, help='path to file transform')
args = parser.parse_args()

filepath = args.i[0]
with open(filepath, 'r') as fh:
    parser = AltoParser(fh)
    print(parser.tojson())

# Pour trier les lignes de tableau étranges :
# Chercher les éléments Illustration (image) et ComposedBlock (TYPE="Table")
# Définir un top-max (=VPOS) et bottom-max (=VPOS+HEIGHT)
# Vérifier que chaque ligne n'est pas compris dans cet espace

with open('./lines.json', 'w') as fl:
    fl.write(parser.tojson())

